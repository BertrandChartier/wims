# Installation WIMS académique

**[Version Beta](http://cite-aborne.ac-grenoble.fr/web/wims/)**

**[RETEX](https://codi-grenoble.beta.education.fr/H9_0F7udRLya8HBoAu2BDA?both)**

## Infra:
WIMS est une plateforme d’enseignement proposant entre autres des exercices et des cours interactifs en accès libre et gratuit.  
[WimsEdu.info](https://wimsedu.info/)

WIMS n’est vraiment pas très gourmand, et un serveur relativement d’entrée de gamme suffit en général amplement.
Celle de WIMS unice (13.400 comptes utilisateurs) a les caractéristiques suivantes :  
**RAM : 4 Gio**  
**CPU : 4 (2 x 2) – 2.27 GHz**  
**HDD : 120 Go**  
[Source](https://wimsedu.info/?topic=installation-serveur-wims-serveur-distant)

Wims fonctionne en tant que script CGI derrière un serveur Web.  
Apache est l'outil préconisé par WIMS, et pour lequel la configuration est automatique.  
L'ensemble des fichiers sont placés dans /opt/wims dont l'utilisateur **wims** est le proprietaire.  
Le fichier /opt/wims/log/.wimspass contient le mot de passe d'accès à la console d'administration fonctionnelle.  
Le fichier /opt/wims/log/wims.conf contient la configuration du site ainsi que les adresses IP autorisées à accéder à la console d'administration fonctionnelle.  

### Partitions
|Dossier|Volume|Type|
|:---------------|:--------------:|:------------|
| / | 10 Go |Primary|
| /home | 5 Go |logique|
| SWAP | 4 Go ||
| /opt | 100 Go |logique|


## PRE-REQUIS
Base **Debian Stretch amd64** sécurisé **FrontWeb**

configuration exim, SSL

**[Pré-configuration Virtual Box](pre_conf_Vbox.sh)**


## Install
Utilisation de la procédure d'installation de ac-versailles (dépot leur dépot).  
Configuration de Apache  
Exclusion du paquet wims des mises à jour.  

**[Installation](install.sh)**

[OLD Source](https://github.com/flaf/debian-pkg-wims)  

## Post-Install
Modification de la restriction du domaine de messagerie  
Placer l'authentification CAS de l'ENT AURA comme choix par défaut  
Copie du fichier de configuration de Wims  
Copie du Skin Ac-Grenoble  
Installation des classes ouvertes COVID

**[Procédure Post-configuration](post_conf.sh)**  

## TODO
* [ ] Définition de l'adresse IP accédant à l'administration fonctionnelle (dans /opt/wims/log/wims.conf)
* [ ] Bug CSS mise en forme menu dans page élève et enseignant
* [ ] Choix des méthodes d'authentification & accès LDAP
* [ ] Bug indexation classe virtuelle
* [ ] Test GAP [Bug GAP](https://wiki.wimsedu.info/doku.php?id=installation:logiciels_particuliers)
* [ ] Test Scilab
* [ ] Test Macaulay2
* [ ] Test restauration sauvegarde
* [ ] Archivage des logs & sauvegardes
* [ ] Script mise à jour Wims


## Depends Wims
wims  
	Pré-Dépend: adduser  
	Pré-Dépend: apache2  
	Pré-Dépend: procps  
	Pré-Dépend: sudo  
		sudo-ldap  
	Dépend: libc6  
	Dépend: libgcc1  
	Dépend: libgd3  
	Dépend: libstdc++6  
	Dépend: yacas  
	Dépend: gap  
	Dépend: maxima  
	Dépend: maxima-share  
	Dépend: octave  
	Dépend: graphviz  
	Dépend: ldap-utils  
	Dépend: scilab-cli  
	Dépend: libwebservice-validator-html-w3c-perl  
	Dépend: qrencode  
	Dépend: <fortune>  
		fortune-mod  
	Dépend: unzip  
	Dépend: libgmp-dev  
	Dépend: openbabel  
	Dépend: povray  
	Dépend: pari-gp2c  
	Dépend: pari-gp  
	Dépend: gnuplot  
		gnuplot-nox  
		gnuplot-qt  
		gnuplot-x11  
	Dépend: texlive-base  
	Dépend: texlive-latex-base  
	Dépend: texlive-binaries  
	Dépend: texlive-fonts-recommended  
	Dépend: chemeq  
	Dépend: libjs-prototype  
	Dépend: libjs-mootools  
	Dépend: libjs-edit-area  
	Dépend: libjs-jquery  
	Dépend: libjs-jquery-metadata  
	Dépend: libjs-asciimathml  
	Dépend: units-filter  
	Dépend: flex  
	Dépend: bison  
	Dépend: liburi-perl  
	Dépend: imagemagick  
		graphicsmagick-imagemagick-compat  
		imagemagick-6.q16  
	Dépend: wget                          

