!!! Variables  Euler theme !!!

!!! Block News  !!!

!set euler_actu= News
!set euler_all_actu= All news
!set euler_modal_actu= News of the year

!!! Block Example  !!!

!set euler_exple_title= Examples of resources
!set euler_exple_desc= Description:
!set euler_exple_level= Level:
!set euler_exple_author= Author(s):


!!! Block Taxo and Glossary  !!!

!set euler_TaxoGlo_title= Taxonomy and Glossary
!set euler_math= Mathematics
!set euler_algebra= Algebra
!set euler_analysis= Analysis
!set euler_arith= Arithmetics
!set euler_gene= General
!set euler_geom= Geometry
!set euler_proba= Probability
!set euler_stat= Statistics

!!! Block Other Resources  !!!

!set euler_other_resources= En este servidor, usted puede encontrar:
!set euler_CRI = Lecciones y referencias interactivas sobre varios temas.
!set euler_OCGL = Calculadoras y representaciones gr�ficas en l�nea: n�meros, funciones, matrices, curvas, superficies, etc...
!set euler_EI = Ejercicios interactivos de varios estilos y niveles.
!set euler_RM = Matem�ticas recreativas: puzzles y juegos.
!set euler_CO = Open classes (or create your own class).

!!! Button Hide/Show

!set euler_btn_show= Noticias-Ejemplos
!set euler_btn_hide= Ocultar
!set euler_btn_hide_show_title= Mostrar/Ocultar noticias y ejemplos


!!! Redefinition of menuitem
!set wims_name_n_supervisor=Profesores
!set wims_name_n_participant =Estudiantes
!set wims_name_aboutw=Acerca