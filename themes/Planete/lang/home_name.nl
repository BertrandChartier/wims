!!! Variables  Euler theme !!!

!!! Block News  !!!

!set euler_actu= News
!set euler_all_actu= All news
!set euler_modal_actu= News of the year

!!! Block Example  !!!

!set euler_exple_title= Examples of resources
!set euler_exple_desc= Description:
!set euler_exple_level= Level:
!set euler_exple_author= Author(s):

!!! Block Taxo and Glossary  !!!

!set euler_TaxoGlo_title= Taxonomy and Glossary
!set euler_math= Mathematics
!set euler_algebra= Algebra
!set euler_analysis= Analysis
!set euler_arith= Arithmetics
!set euler_gene= General
!set euler_geom= Geometry
!set euler_proba= Probability
!set euler_stat= Statistics

!!! Block Other Resources  !!!

!set euler_other_resources= Op deze website, vind je
!set euler_CRI = Lesmateriaal en verwijzingen naar diverse onderwerpen.
!set euler_OCGL = Online rekenmachines en functie plotters : getallen, functies, matrices, krommen, oppervlakken, etc...
!set euler_EI = Interactieve oefeningen van verschillende stijl en moeilijkheidsgraad.
!set euler_RM = Wiskundige ontspanning : puzzels en spelletjes.
!set euler_CO = Openbare virtuele klaslokalen.

!!! Button Hide/Show

!set euler_btn_show= Nieuws-Voorbeelden
!set euler_btn_hide= Verbergen
!set euler_btn_hide_show_title= Toon/Verbergen nieuws en voorbeelden


!!! Redefinition of menuitem
!set wims_name_aboutw=About